<?php

namespace athc\pollgaugemeter\acp;

class widget_info
{
	public function module()
	{
		return array(
			'filename'  => '\athc\pollgaugemeter\acp\widget_module',
			'title'     => 'ACP_POLLGAUGEMETER_TITLE',
			'modes'    => array(
				'settings'  => array(
					'title' => 'ACP_POLLGAUGEMETER_SETTINGS',
					'auth'  => 'ext_athc/pollgaugemeter && acl_a_board',
					'cat'   => array('ACP_POLLGAUGEMETER_TITLE'),
				),
			),
		);
	}
}