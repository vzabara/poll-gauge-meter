<?php
/**
 *
 * This file is part of the phpBB Forum Software package.
 *
 * @copyright (c) phpBB Limited <https://www.phpbb.com>
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 * For full copyright and license information, please see
 * the docs/CREDITS.txt file.
 *
 */

namespace athc\pollgaugemeter\event;

/**
 * @ignore
 */
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event listener
 */
class widget_listener implements EventSubscriberInterface
{
	static public function getSubscribedEvents()
	{
		return array(
			'core.user_setup' => array(array('load_language_on_setup')),
			'core.page_header' => 'get_poll_results',
		);
	}

	/* @var \phpbb\controller\helper */
	protected $helper;

	/* @var \phpbb\template\template */
	protected $template;

	/* @var \phpbb\db\driver\driver_interface */
	protected $db;

	/**
	 * Constructor
	 *
	 * @param \phpbb\controller\helper $helper   Controller helper object
	 * @param \phpbb\template\template $template Template object
	 */
	public function __construct(\phpbb\controller\helper $helper, \phpbb\template\template $template, \phpbb\db\driver\driver_interface $db)
	{
		$this->helper = $helper;
		$this->template = $template;
		$this->db = $db;
	}

	/**
	 * Load the Acme Demo language file
	 *     acme/demo/language/en/demo.php
	 *
	 * @param \phpbb\event\data $event The event object
	 */
	public function load_language_on_setup($event)
	{
		$lang_set_ext = $event['lang_set_ext'];
		$lang_set_ext[] = array(
			'ext_name' => 'athc/pollgaugemeter',
			'lang_set' => 'pollgaugemeter',
		);
		$event['lang_set_ext'] = $lang_set_ext;
	}

	/**
	 * Get poll results
	 */
	public function get_poll_results()
	{
		global $request;

        $parts = pathinfo($request->server('SCRIPT_NAME'));
        if (in_array($parts['basename'], $this->allowedPages())) {
            $forum_id = $request->variable('f', 0);
            $topic_id = $request->variable('t', 0);
            $total = 0;
            $up = 0;
            $positive = 0;
            $sql = 'SELECT * FROM `' . POLL_OPTIONS_TABLE . '` 
                WHERE topic_id = "' . $this->db->sql_escape($topic_id) . '"';
			$result = $this->db->sql_query($sql);
			while ($row = $this->db->sql_fetchrow($result)) {
                if (strtolower($row['poll_option_text']) == 'up') {
                    $total += $row['poll_option_total'];
                    $positive = $row['poll_option_total'];
                } else if (strtolower($row['poll_option_text']) == 'down') {
                    $total += $row['poll_option_total'];
                }
            }
			$this->db->sql_freeresult($result);
            if ($positive) {
                $up = ceil(($positive * 100) / $total);
            }
		}
        $this->template->assign_var('TOTAL_POLL_RESULTS', $total);
        $this->template->assign_var('POLL_RESULTS_UP', $up);
        $this->template->assign_var('FORUM_ID', $forum_id);
        $this->template->assign_var('TOPIC_ID', $topic_id);
	}

    private function allowedPages()
    {
        return array(
            'viewtopic.php',
        );
    }
}
