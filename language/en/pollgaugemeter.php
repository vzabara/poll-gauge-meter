<?php

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'POLLGAUGEMETER_PAGE'              => 'Poll Gauge Meter Widget',
	'ACP_POLLGAUGEMETER_TITLE'         => 'Poll Gauge Meter Widget',
	'ACP_POLLGAUGEMETER_EXPLAIN'       => '',
	'ACP_POLLGAUGEMETER_WIDGETS'       => 'Poll Gauge Meter Widgets',
	'ACP_POLLGAUGEMETER_SETTINGS'      => 'Poll Gauge Meter Widgets Settings',
	'ACP_POLLGAUGEMETER_SAVED'         => 'Record have been saved successfully!',
	'EDIT_WIDGET'                  => 'Edit Widget',
	'ADD_WIDGET'                   => 'Add Widget',
	'NO_WIDGET'                    => 'Coin widget was not found',
	'ACP_NO_WIDGETS'               => 'Coin widget was not found',
	'FORM_INVALID'                 => 'Something wrong with form data',
	'ENTER_DATA'                   => 'Data was not set',
	'WIDGET_UPDATED'               => 'Coin widget was updated',
	'WIDGET_ADDED'                 => 'Coin widget was added',
	'WIDGET_REMOVED'               => 'Coin widget was removed',
	'FORUM_TITLE'                  => 'Forum title',
	'TOPIC_TITLE'                  => 'Topic title',
	'COIN_WIDGET'                  => 'Coin widget',
));