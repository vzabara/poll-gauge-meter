<?php
/**
* @ignore
*/
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

// Initial var setup
$forum_id	= $request->variable('f', 0);
$topic_id	= $request->variable('t', 0);

$total = 0;
$up = 0;
$positive = 0;
if ($topic_id)
{
    $sql = 'SELECT * FROM `' . POLL_OPTIONS_TABLE . '` 
                WHERE topic_id = "' . $db->sql_escape($topic_id) . '"';
    $result = $db->sql_query($sql);
    while ($row = $db->sql_fetchrow($result)) {
        if (strtolower($row['poll_option_text']) == 'up') {
            $total += $row['poll_option_total'];
            $positive = $row['poll_option_total'];
        } else if (strtolower($row['poll_option_text']) == 'down') {
            $total += $row['poll_option_total'];
        }
    }
    $db->sql_freeresult($result);
    if ($positive) {
        $up = ceil(($positive * 100) / $total);
    }
}
$data = [
    'total' => $total,
    'up' => $up,
];
$json_response = new \phpbb\json_response();
$json_response->send($data);

