var gaugeOptions = {

	chart: {
		type: 'solidgauge'
	},

	title: null,

	pane: {
		center: ['50%', '85%'],
		size: '140%',
		startAngle: -90,
		endAngle: 90,
		background: {
			backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
			innerRadius: '60%',
			outerRadius: '100%',
			shape: 'arc'
		}
	},

	tooltip: {
		enabled: false
	},

	// the value axis
	yAxis: {
		stops: [
			[0.1, '#DF5353'], // red
			[0.5, '#DDDF0D'], // yellow
			[0.9, '#55BF3B'], // green
		],
		lineWidth: 0,
		minorTickInterval: null,
		tickAmount: 2,
		title: {
			y: -70
		},
		labels: {
			y: 16
		}
	},

	plotOptions: {
		solidgauge: {
			dataLabels: {
				y: 5,
				borderWidth: 0,
				useHTML: true
			}
		}
	}
};

// The poll results gauge
var pollResultsGauge = Highcharts.chart('container-poll-results', Highcharts.merge(gaugeOptions, {
	yAxis: {
		min: 0,
		max: 100,
		title: {
			text: 'Total Poll Results: ' + totalPollResults
		}
	},

	credits: {
		enabled: false
	},

	series: [{
		name: 'Up',
		data: [pollResults],
		dataLabels: {
			format: '<div style="text-align:center"><span style="font-size:12px;color:silver">Up: </span><span style="font-size:24px;color:' +
			((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'green') + '">{y}%</span></div>',
			backgroundColor: '#FFF',
			borderColor: '#FFF'
		},
	}]

}));

$("span.poll_total_vote_cnt").on("DOMSubtreeModified", function () {
	$.ajax({
		url: 'pollresults.php',
		data: {
			f: forumId,
			t: topicId
		},
		success: function(data) {
			var point = pollResultsGauge.series[0].points[0];
			point.update(data.up);
			pollResultsGauge.yAxis[0].setTitle({text: 'Total Poll Results: ' + data.total})
		}
	});
});
